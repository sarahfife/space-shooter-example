#pragma once
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <vector>
#include "Bullet.h"

class Enemy
{
public: // access level (to be discussed later)

	// Constructor
	Enemy(sf::Texture& newTexture, 
		sf::Vector2u newScreenBounds, 
		std::vector<Bullet>& newBullets, 
		sf::Texture& newBulletTexture, 
		sf::SoundBuffer& firingSoundBuffer,
		sf::Vector2f startingPosition,
		std::vector<sf::Vector2f> newPattern);

	// Functions to call specific code
	void Update(sf::Time frameTime);
	void DrawTo(sf::RenderTarget& target);

	// Getters
	bool GetAlive();
	sf::FloatRect GetHitbox();

	// Setters
	void SetAlive(bool newAlive);

private:

	// Variables (data members) used by this class
	sf::Sprite sprite;
	sf::Vector2f velocity;
	float speed;
	sf::Vector2u screenBounds;
	std::vector<Bullet>* bullets;
	sf::Texture* bulletTexture;
	sf::Time bulletCooldownRemaining;
	sf::Time bulletCooldownMax;
	sf::Sound bulletFireSound;
	std::vector<sf::Vector2f> movementPattern;
	sf::Vector2f patternStart;
	int currentInstruction;
	bool alive;

};

