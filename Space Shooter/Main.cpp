// Library Includes	
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <string>
#include <vector>
#include <cstdlib>
#include <time.h>
#include "Player.h"
#include "Star.h"
#include "Enemy.h"
#include "SpawnData.h"

int main()
{
	// Game window setup
	sf::RenderWindow gameWindow;
	gameWindow.create(sf::VideoMode::getDesktopMode(), "Space Shooter", sf::Style::None);
	gameWindow.setMouseCursorVisible(false);

	// -----------------------------------------------
	// Game Setup
	// -----------------------------------------------
	// Game Clock
	sf::Clock gameClock;

	// Seed the random number generator
	srand(time(NULL));

	// Bullets
	std::vector<Bullet> playerBullets;
	std::vector<Bullet> enemyBullets;
	sf::Texture playerBulletTexture;
	playerBulletTexture.loadFromFile("Assets/Graphics/playerBullet.png");
	sf::Texture enemyBulletTexture;
	enemyBulletTexture.loadFromFile("Assets/Graphics/enemyBullet.png");

	// Player
	sf::Texture playerTexture;
	playerTexture.loadFromFile("Assets/Graphics/player.png");
	sf::SoundBuffer firingSoundBuffer;
	firingSoundBuffer.loadFromFile("Assets/Audio/fire.ogg");
	Player playerObject(playerTexture, gameWindow.getSize(), playerBullets, playerBulletTexture, firingSoundBuffer);

	// Stars
	sf::Texture starTexture;
	starTexture.loadFromFile("Assets/Graphics/star.png");
	std::vector<Star> stars;
	int numStars = 5;
	for (int i = 0; i < numStars; ++i)
	{
		stars.push_back(Star(starTexture, gameWindow.getSize()));
	}

	// Movement patterns
	std::vector<sf::Vector2f> zigzagPattern;
	zigzagPattern.push_back(sf::Vector2f(-300, 300));
	zigzagPattern.push_back(sf::Vector2f(-600, -300));
	zigzagPattern.push_back(sf::Vector2f(-900, 300));
	zigzagPattern.push_back(sf::Vector2f(-1200, -300));
	std::vector<sf::Vector2f> squarePattern;
	squarePattern.push_back(sf::Vector2f(-500, 0));
	squarePattern.push_back(sf::Vector2f(-500, -500));
	squarePattern.push_back(sf::Vector2f(0, -500));

	// Enemies
	sf::Texture enemyTexture;
	enemyTexture.loadFromFile("Assets/Graphics/enemy.png");
	std::vector<Enemy> enemies;
	sf::SoundBuffer explosionSoundBuffer;
	explosionSoundBuffer.loadFromFile("Assets/Audio/explosion.ogg");
	sf::Sound explosionSound(explosionSoundBuffer);

	// Spawn Info
	int spawnIndex = 0;
	std::vector<SpawnData> spawnData;
	spawnData.push_back({ 
		sf::Vector2f(gameWindow.getSize().x - 100, 100) ,
		zigzagPattern ,
		sf::seconds(1.0f) 
	});
	spawnData.push_back({
		sf::Vector2f(gameWindow.getSize().x - 100, 200) ,
		zigzagPattern ,
		sf::seconds(1.0f)
		});
	spawnData.push_back({
		sf::Vector2f(gameWindow.getSize().x - 100, 300) ,
		zigzagPattern ,
		sf::seconds(1.0f)
		});
	spawnData.push_back({
		sf::Vector2f(gameWindow.getSize().x - 100, 400) ,
		zigzagPattern ,
		sf::seconds(1.0f)
		});
	spawnData.push_back({
		sf::Vector2f(gameWindow.getSize().x - 100, 500) ,
		zigzagPattern ,
		sf::seconds(1.0f)
		});
	spawnData.push_back({
		sf::Vector2f(gameWindow.getSize().x - 100, 600) ,
		squarePattern ,
		sf::seconds(1.0f)
		});
	spawnData.push_back({
		sf::Vector2f(gameWindow.getSize().x - 100, 700) ,
		squarePattern ,
		sf::seconds(1.0f)
		});
	spawnData.push_back({
		sf::Vector2f(gameWindow.getSize().x - 100, 800) ,
		squarePattern ,
		sf::seconds(1.0f)
		});
	sf::Time timeToSpawn = spawnData[spawnIndex].delay;

	// Game Font
	// Declare a font variable called gameFont
	sf::Font gameFont;
	// Load up the font from a file path
	gameFont.loadFromFile("Assets/Fonts/mainFont.ttf");

	// Score
	int score = 0;
	// Setup score text object
	sf::Text scoreText;
	scoreText.setFont(gameFont);
	scoreText.setString("Score: 0");
	scoreText.setCharacterSize(64);
	scoreText.setFillColor(sf::Color::White);
	scoreText.setPosition(30, 30);

	// Game Music
	// Declare a music variable called gameMusic
	sf::Music gameMusic;
	// Load up our audio from a file path
	gameMusic.openFromFile("Assets/Audio/music.ogg");
	// Start our music playing
	gameMusic.play();

	// Game over variable to track if the game is done
	bool gameOver = false;

	// Game Over Text
	sf::Text gameOverText;
	gameOverText.setFont(gameFont);
	gameOverText.setString("GAME OVER\n\nress R to restart\nor Q to quit");
	gameOverText.setCharacterSize(72);
	gameOverText.setFillColor(sf::Color::Cyan);
	gameOverText.setStyle(sf::Text::Bold | sf::Text::Italic);
	gameOverText.setPosition(gameWindow.getSize().x / 2 - gameOverText.getLocalBounds().width / 2, 150);

	// Game end SFX
	sf::SoundBuffer lossSB;
	lossSB.loadFromFile("Assets/Audio/loss.ogg");
	sf::Sound lossSound(lossSB);
	sf::SoundBuffer winSB;
	winSB.loadFromFile("Assets/Audio/win.ogg");
	sf::Sound winSound(winSB);

	// -----------------------------------------------
	// Game Loop
	// -----------------------------------------------
	// Repeat as long as the window is open
	while (gameWindow.isOpen())
	{


		// -----------------------------------------------
		// Input Section
		// -----------------------------------------------
		// Declare a variable to hold an Event, called gameEvent
		sf::Event gameEvent;
		// Loop through all events and poll them, putting each one into our gameEvent variable
		while (gameWindow.pollEvent(gameEvent))
		{
			// This section will be repeated for each event waiting to be processed

			// Did the player try to close the window?
			if (gameEvent.type == sf::Event::Closed)
			{
				// If so, call the close function on the window.
				gameWindow.close();
			}
		} // End event polling loop

		// Close game if escape is pressed
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
		{
			// If so, call the close function on the window.
			gameWindow.close();
		}

		// Only do these things if the game is not over
		if (!gameOver)
		{
			// Player keybind input
			playerObject.Input();
		}

		// Check if we should reset or quit the game
		if (gameOver)
		{
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::R))
			{
				// Reset the game
				score = 0;
				scoreText.setString("Score: " + std::to_string(score));
				gameMusic.play();
				gameOver = false;
				playerObject.Reset();
				enemies.clear();
				enemyBullets.clear();
				playerBullets.clear();
				spawnIndex = 0;
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Q))
			{
				gameWindow.close();
			}
		}


		// -----------------------------------------------
		// Update Section
		// -----------------------------------------------
		// Get the time passed since the last frame and restart our game clock
		sf::Time frameTime = gameClock.restart();

		// Only do these things if the game is not over
		if (!gameOver)
		{
			// Update the player
			playerObject.Update(frameTime);

			// Update the stars
			for (int i = 0; i < stars.size(); ++i)
			{
				stars[i].Update(frameTime);
			}

			// Update the bullets
			for (int i = playerBullets.size() - 1; i >= 0; --i)
			{
				playerBullets[i].Update(frameTime);

				// If the bullet is dead, delete it
				if (!playerBullets[i].GetAlive())
				{
					// Remove the bullet from the vector
					playerBullets.erase(playerBullets.begin() + i);
				}
			}
			for (int i = enemyBullets.size() - 1; i >= 0; --i)
			{
				enemyBullets[i].Update(frameTime);

				// If the bullet is dead, delete it
				if (!enemyBullets[i].GetAlive())
				{
					// Remove the bullet from the vector
					enemyBullets.erase(enemyBullets.begin() + i);
				}
			}

			// Update the enemies
			for (int i = enemies.size() - 1; i >= 0; --i)
			{
				enemies[i].Update(frameTime);

				// If the enemy is dead, delete it
				if (!enemies[i].GetAlive())
				{
					// Remove the enemy from the vector
					enemies.erase(enemies.begin() + i);
				}
			}

			// Check if it is time to spawn an enemy
			timeToSpawn -= frameTime;
			if (spawnIndex < spawnData.size() && timeToSpawn <= sf::seconds(0))
			{
				enemies.push_back(Enemy(
					enemyTexture,
					gameWindow.getSize(),
					enemyBullets,
					enemyBulletTexture,
					firingSoundBuffer,
					spawnData[spawnIndex].position,
					spawnData[spawnIndex].pattern
				));
				++spawnIndex;
				if (spawnIndex < spawnData.size())
					timeToSpawn = spawnData[spawnIndex].delay;
			}

			// Check collision with player bullets and enemies
			for (int i = 0; i < playerBullets.size(); ++i)
			{
				for (int j = 0; j < enemies.size(); ++j)
				{
					sf::FloatRect bulletHitbox = playerBullets[i].GetHitbox();
					sf::FloatRect enemyHitbox = enemies[j].GetHitbox();
					if (bulletHitbox.intersects(enemyHitbox))
					{
						// Kill both the enemy and the bullet
						playerBullets[i].SetAlive(false);
						enemies[j].SetAlive(false);

						// Score!
						score += 100; // We only have one type of enemy so this is okay
						// Better would be to store score on the enemy so it could be different for different enemies!

						// Update our score display text based on our current numerical score
						scoreText.setString("Score: " + std::to_string(score));

						explosionSound.play();
					}
				}
			}

			// Check collision with enemy bullets and player
			for (int i = 0; i < enemyBullets.size(); ++i)
			{
				sf::FloatRect bulletHitbox = enemyBullets[i].GetHitbox();
				sf::FloatRect playerHitbox = playerObject.GetHitbox();
				if (bulletHitbox.intersects(playerHitbox))
				{
					// Kill the bullet
					enemyBullets[i].SetAlive(false);

					// End the game
					gameOver = true;
					gameMusic.stop();
					lossSound.play();
					// Set the string of text that will be displayed by this text object
					gameOverText.setString("GAME OVER\n\nPress R to restart\nor Q to quit");
					// Position our text in the top center of the screen
					gameOverText.setPosition(gameWindow.getSize().x / 2 - gameOverText.getLocalBounds().width / 2, 150);
				}
			}

			// Check if we have finished the last wave
			if (spawnIndex >= spawnData.size() && enemies.size() == 0)
			{
				// we won!
				gameOver = true;
				gameMusic.stop();
				winSound.play();
				// Set the string of text that will be displayed by this text object
				gameOverText.setString("YOU WIN!\n\nPress R to restart\nor Q to quit");
				// Position our text in the top center of the screen
				gameOverText.setPosition(gameWindow.getSize().x / 2 - gameOverText.getLocalBounds().width / 2, 150);
			}
		}

		// -----------------------------------------------
		// Draw Section
		// -----------------------------------------------
		// Clear the window to a single colour
		gameWindow.clear(sf::Color(20, 20, 20));


		// Draw everything to the window

		// Only draw these if the game is not over
		if (!gameOver)
		{
			// Draw the stars
			for (int i = 0; i < stars.size(); ++i)
			{
				stars[i].DrawTo(gameWindow);
			}

			// Draw the bullets
			for (int i = 0; i < playerBullets.size(); ++i)
			{
				playerBullets[i].DrawTo(gameWindow);
			}
			for (int i = 0; i < enemyBullets.size(); ++i)
			{
				enemyBullets[i].DrawTo(gameWindow);
			}

			// Draw the enemies
			for (int i = 0; i < enemies.size(); ++i)
			{
				enemies[i].DrawTo(gameWindow);
			}

			playerObject.DrawTo(gameWindow);
		}
		// Only draw if the game IS over!
		else
		{
			gameWindow.draw(gameOverText);
		}

		// UI draws last so it's on top
		gameWindow.draw(scoreText);

		// Display the window contents on the screen
		gameWindow.display();

	} // End of Game Loop


	return 0;
}