#include "Enemy.h"

Enemy::Enemy(sf::Texture& newTexture,
	sf::Vector2u newScreenBounds,
	std::vector<Bullet>& newBullets,
	sf::Texture& newBulletTexture,
	sf::SoundBuffer& firingSoundBuffer,
	sf::Vector2f startingPosition,
	std::vector<sf::Vector2f> newPattern)
	: sprite(newTexture)
	, velocity(0.0f, 0.0f)
	, speed(300.0f)
	, screenBounds(newScreenBounds)
	, bullets(&newBullets)
	, bulletTexture(&newBulletTexture)
	, bulletCooldownRemaining(sf::seconds(0.0f))
	, bulletCooldownMax(sf::seconds(0.5f))
	, bulletFireSound(firingSoundBuffer)
	, movementPattern(newPattern)
	, patternStart(startingPosition)
	, currentInstruction(0)
	, alive(true)
{
	sprite.setPosition(startingPosition);
}

void Enemy::Update(sf::Time frameTime)
{
	// if we are past the last instruction in the pattern, 
	// set ourselves as dead and do nothing else
	if (currentInstruction >= movementPattern.size())
	{
		alive = false;
		return;
	}

	// Current instruction
	sf::Vector2f currentTarget = patternStart + movementPattern[currentInstruction];

	// Determine direction
	sf::Vector2f newDirection = currentTarget - sprite.getPosition();

	// Direction should be a unit vector (length 1)
	// To get that, divide each element by magnitude (length) of vector
	float magnitude = std::sqrtf(newDirection.x * newDirection.x + newDirection.y * newDirection.y);
	newDirection.x = newDirection.x / magnitude;
	newDirection.y = newDirection.y / magnitude;

	// Velocity = direction * speed
	velocity = newDirection * speed;

	// Calculate how far we will move
	sf::Vector2f moveVector = velocity * frameTime.asSeconds();

	// Calculate how far away the target is
	sf::Vector2f distanceVector = currentTarget - sprite.getPosition();

	// Compare square magnitudes, if the square magnitude for moveVector is higher, we would overshoot,so just move to target
	float moveSqrMag = moveVector.x * moveVector.x + moveVector.y * moveVector.y;
	float distSqrMag = distanceVector.x * distanceVector.x + distanceVector.y * distanceVector.y;
	if (moveSqrMag >= distSqrMag)
	{
		// We reached our destination!

		// Move the player to the the target position so we don't overshoot
		sprite.setPosition(currentTarget);

		// Update to the next instruction
		++currentInstruction;
	}
	else
	{
		// We haven't reached our destination yet, keep going.

		// Calculate the new position
		sf::Vector2f newPosition = sprite.getPosition() + velocity * frameTime.asSeconds();

		// Move the player to the new position
		sprite.setPosition(newPosition);
	}

	// Update the cooldown remaining for firing bullets
	bulletCooldownRemaining -= frameTime;

	// Fire bullets if it is time
	if (bulletCooldownRemaining <= sf::seconds(0.0f))
	{
		sf::Vector2f bulletPosition = sprite.getPosition();
		bulletPosition.y += sprite.getTexture()->getSize().y / 2 - bulletTexture->getSize().y / 2;
		bulletPosition.x += sprite.getTexture()->getSize().x / 2 - bulletTexture->getSize().x / 2;
		bullets->push_back(Bullet(*bulletTexture, screenBounds, bulletPosition, sf::Vector2f(-1000, 0)));

		// reset bullet cooldown
		bulletCooldownRemaining = bulletCooldownMax;

		// Play firing sound
		bulletFireSound.play();
	}
}

void Enemy::DrawTo(sf::RenderTarget& target)
{
	target.draw(sprite);
}

bool Enemy::GetAlive()
{
	return alive;
}

sf::FloatRect Enemy::GetHitbox()
{
	return sprite.getGlobalBounds();
}

void Enemy::SetAlive(bool newAlive)
{
	alive = newAlive;
}