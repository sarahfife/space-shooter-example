#pragma once
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <vector>
#include "Bullet.h"

class Player
{
public: // access level (to be discussed later)

	// Constructor
	Player(sf::Texture& playerTexture, sf::Vector2u newScreenBounds, std::vector<Bullet>& newBullets, sf::Texture& newBulletTexture, sf::SoundBuffer& firingSoundBuffer);

	// Functions to call Player-specific code
	void Input();
	void Update(sf::Time frameTime);
	void Reset();
	void DrawTo(sf::RenderTarget& target);

	// Getters
	sf::FloatRect GetHitbox();

private:

	// Variables (data members) used by this class
	sf::Sprite sprite;
	sf::Vector2f velocity;
	float speed;
	sf::Vector2u screenBounds;
	std::vector<Bullet>& bullets;
	sf::Texture& bulletTexture;
	sf::Time bulletCooldownRemaining;
	sf::Time bulletCooldownMax;
	sf::Sound bulletFireSound;

};

